package ru.tsc.babeshko.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.api.repository.IUserOwnedRepository;
import ru.tsc.babeshko.tm.api.service.IConnectionService;
import ru.tsc.babeshko.tm.api.service.IUserOwnedService;
import ru.tsc.babeshko.tm.dto.model.AbstractUserOwnedModelDTO;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModelDTO, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

}