package ru.tsc.babeshko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.enumerated.Status;
import ru.tsc.babeshko.tm.dto.model.TaskDTO;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IUserOwnedService<TaskDTO> {

    void create(@NotNull String userId, @NotNull String name);

    void create(@NotNull String userId, @NotNull String name, @NotNull String description);

    void create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    );

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    TaskDTO updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description);

    @NotNull
    TaskDTO changeTaskStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status);

    void removeAllByProjectId(String userId, String projectId);

}