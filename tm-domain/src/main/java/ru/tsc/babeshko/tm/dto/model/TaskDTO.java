package ru.tsc.babeshko.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
public final class TaskDTO extends AbstractUserOwnedModelDTO {

    private static final long serialVersionUID = 1;

    @Nullable
    @Column(name = "project_id")
    private String projectId = null;

}